﻿using System;
using Microsoft.EntityFrameworkCore;

namespace LMS.Model
{
    public class LMSDBContext : DbContext
    {

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrolment> Enrolments { get; set; }
        public LMSDBContext(DbContextOptions<LMSDBContext> options) :base(options)
        {
            Database.EnsureCreated();
            //Database.Migrate();

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            //Course

            //Assignment
           
            //Student                
           
            //Student Detail               
           
            //Student Country

            //Student Address

            //Enrollment
           
            //Lecturer

            //Lecturer Detail

            //Teaching




            // To ignore a class :modelBuilder.Ignore<StudentAddress>();

            /*
            modelBuilder.Entity<Student>()
                        .HasOne(a => a.StudentAddress)
                        .WithOne(b => b.Student);
                        */
        }


       protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
       {
            // DB setting is set here 
            var connectionString = "server=localhost;userid=root;pwd=;port=3306;database=LMSTeamA;sslmode=none;";
            optionsBuilder.UseMySQL(connectionString);
            base.OnConfiguring(optionsBuilder);
       }

    }
}

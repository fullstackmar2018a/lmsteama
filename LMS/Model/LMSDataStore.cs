﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace LMS.Model
{
    public class LMSDataStore:ILMSDataStore
    {
        private LMSDBContext _ctx;

        public LMSDataStore(LMSDBContext ctx)
        {
            _ctx = ctx;
        }
        // For Course API 

        // For Student API 

        // For Enrolment API 

        // For Lecturer API





        public bool Save()
        {
            //True for success , False should throw exception
            return (_ctx.SaveChanges() >= 0);
        }

    }
}

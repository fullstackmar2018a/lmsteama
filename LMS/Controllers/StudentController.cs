﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using LMS.Model;
namespace LMS.Controllers
{
    [Route("api/student")]
    public class StudentController : Controller
    {

        private ILMSDataStore _dbstore;
        public StudentController(ILMSDataStore dbstore)
        {
            _dbstore = dbstore;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _dbstore.GetAllStudent();
            //sonResult resultJSObj = new JsonResult(result);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post([FromBody]Student student)
        {
            Student newCourse = Student.CreateNewStudentFromBody(student);
            _dbstore.AddStudent(newCourse);
            _dbstore.Save();
            return Ok();
        }

        /*
        [HttpPut("{id}")]
        public IActionResult Put(int styd, [FromBody]StudentDetail detail)
        {
            _dbstore.EditCourse(courseID, newCourse);
            return Ok();
        }*/
    }
}
